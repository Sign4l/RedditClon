from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from datetime import datetime


def hora_actual(request):
	"""
	hora = datetime.now()
	t = get_template("hora.html")
	c = Context({"usuario": "Jordi", "hora": hora})
	html = t.render(c)
	return HttpResponse(html)
	"""

#   -->  Modo corto
	now = datetime.now()
	return render_to_response('hora.html', {'hora': now})
