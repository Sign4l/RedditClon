from __future__ import unicode_literals
from django.db import models

# Creado la app puls3, que contiene categorias, enlaces y votos

class Categoria(models.Model):
	titulo = models.CharField(max_length = 140)

class Link(models.Model):
	titulo = models.CharField(max_length = 140, default="Hola")
	enlace = models.URLField(blank=True)
	votos = models.IntegerField(default=0)
# modelos relacionales (relacion entre objecto1 y objeto2 o entre clases)
	categoria = models.ForeignKey(Categoria)


    

